import { Request, Response } from "express";
import { bookModel } from "../models/book";
import { badRequest, dataNotFound, internalError } from "../services/helper";

const getBooks = (request: Request, response: Response) => {
    let subject: any = request.query.subject ?? 'love'
    let page: any = request.query.page ?? 1
    let limit: any = request.query.limit ?? 10
    {
        if( isNaN(+page) || isNaN(+limit)) {
            return badRequest(response, 'invalid query string value!')
        }
    }
    page = parseInt(page)
    limit = parseInt(limit)
    bookModel.getBooks(subject, limit, page).then((data) => {
        if(data) {
            return response.json({data})
        } else {
            return dataNotFound(response, 'data not found.')
        }
    }).catch(err => internalError(response, err))
}

const getBook = (request: Request, response: Response) => {
    const id = request.params.id
    console.log(id)
    {
        if(!id) return badRequest(response, 'invalid id!')
    }

    bookModel.getBook(id).then((book) => {
        if(book) return response.json(book)
        else return dataNotFound(response, 'data not found.')
    }).catch(err => internalError(response, err))
}

export const bookController = {
    getBooks,
    getBook,
}