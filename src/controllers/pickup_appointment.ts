import { Request, Response } from "express";
import { PickupAppointment, pickupAppointmentModel} from "../models/pickup_appointment";
import { badRequest, dataNotFound, internalError } from "../services/helper";

const getPickupAppointments = (request: Request, response: Response) => {
    let page: any = request.query.page ?? 1
    let limit: any = request.query.limit ?? 10
    {
        if( isNaN(+page) || isNaN(+limit)) {
            return badRequest(response, 'invalid query string value!')
        }
    }
    page = parseInt(page)
    limit = parseInt(limit)
    let offset = (page - 1) * limit
    pickupAppointmentModel.getPickupAppointments(offset, limit).then((pickupAppointments) => {
        pickupAppointmentModel.getTotal().then((total) => {
            return response.json({
                pickupAppointments: pickupAppointments,
                page: page,
                limit: limit,
                total: total,
                total_page: Math.ceil(total / limit),
                total_display: pickupAppointments.length
            })
        }).catch(err => internalError(response, err))
    }).catch(err => internalError(response, err))
}

const getPickupAppointment = (request: Request, response: Response) => {
    const id = parseInt(request.params.id)
    {
        if(id < 0) return badRequest(response, 'invalid id!')
    }

    pickupAppointmentModel.getPickupAppointment(id).then((pickupAppointment) => {
        if(pickupAppointment) return response.json(pickupAppointment)
        else return dataNotFound(response, 'data not found.')
    }).catch(err => internalError(response, err))
}

const insertPickupAppointment = async (request: Request, response: Response) => {
    {
        const pickupAppointment = request.body
        if(!pickupAppointment) return badRequest(response, 'Invalid')
        if(!pickupAppointment.title) return badRequest(response, 'title cannot be empty!')
        if(!pickupAppointment.author) return badRequest(response, 'author cannot be empty!')
        if(!pickupAppointment.edition_number) return badRequest(response, 'edition_number cannot be empty!')
        if(!pickupAppointment.pickup_time) return badRequest(response, 'pickup_time cannot be empty!')

        const pickupAppointmentExist = await pickupAppointmentModel.getExistingPickupAppointment(pickupAppointment)
        if(pickupAppointmentExist) return badRequest(response, 'clashing with existing pickupAppointment!')
    }

    const pickupAppointment = request.body as PickupAppointment
    pickupAppointmentModel.insertPickupAppointment(pickupAppointment).then((pickupAppointment) => {
        return response.json({pickupAppointment})
    }).catch(err => internalError(response, err))
}

const updatePickupAppointment = async (request: Request, response: Response) => {
    const id = parseInt(request.params.id)
    {
        if(id < 0) return badRequest(response, 'invalid id!')
        
        const pickupAppointment = request.body
        if(!pickupAppointment) return badRequest(response, 'Invalid')
        if(!pickupAppointment.title) return badRequest(response, 'title cannot be empty!')
        if(!pickupAppointment.author) return badRequest(response, 'author cannot be empty!')
        if(!pickupAppointment.edition_number) return badRequest(response, 'edition_number cannot be empty!')
        if(!pickupAppointment.pickup_time) return badRequest(response, 'pickup_time cannot be empty!')

        const pickupAppointmentToUpdate = await pickupAppointmentModel.getPickupAppointment(id)
        if(!pickupAppointmentToUpdate) return dataNotFound(response, 'data not found.')

        const pickupAppointmentExist = await pickupAppointmentModel.getExistingPickupAppointment(pickupAppointment)
        if(pickupAppointmentExist) return badRequest(response, 'clashing with existing pickupAppointment!')
    }

    const pickupAppointment = request.body as PickupAppointment
    pickupAppointment.id = id
    pickupAppointmentModel.updatePickupAppointment(pickupAppointment).then((pickupAppointment) => {
        return response.json({pickupAppointment})
    }).catch(err => internalError(response, err))
}

const deletePickupAppointment = async (request: Request, response: Response) => {
    const id = parseInt(request.params.id)
    {
        if(id < 0) return badRequest(response, 'invalid id!')

        const pickupAppointmentToDelete = await pickupAppointmentModel.getPickupAppointment(id)
        if(!pickupAppointmentToDelete) return dataNotFound(response, 'data not found.')
    }

    pickupAppointmentModel.deletePickupAppointment(id).then(() => {
        return response.sendStatus(200)
    }).catch(err => internalError(response, err))
}

export const pickupAppointmentController = {
    getPickupAppointments,
    getPickupAppointment,
    insertPickupAppointment,
    updatePickupAppointment,
    deletePickupAppointment
}