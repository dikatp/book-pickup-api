import sqlite3 from 'sqlite3'
import {getDateTimeString} from './helper'

const DBSOURCE = process.env.DBSOURCE
if(!DBSOURCE) throw new Error('DB NOT FOUND')

export const openConn = () => {
    let db = new sqlite3.Database(DBSOURCE)
    return db
}

export const query = async (queryString: string, params?: any[]) => {
    let db = openConn()
    try {
        return await new Promise<any[]>((resolve, reject) => {
            db.all(queryString, params, (error, rows) => {
                if (error)
                    reject(error)
                else
                    resolve(rows)
            })
        })
    } finally {
        db.close()
    }
}

export const selectFirst = async (queryString: string, params?: any[]) => {
    const queryResult = await query(queryString, params)
    return queryResult[0]
}

export const initData = () => {
    console.log('Initiate Data')
    let db = openConn()
    db.run(`CREATE TABLE pickup_appointments (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        title TEXT NOT NULL,
        author TEXT NOT NULL,
        edition_number TEXT NOT NULL,
        pickup_time DATETIME NOT NULL,
        created_at DATETIME DEFAULT (CURRENT_TIMESTAMP),
        is_deleted TEXT DEFAULT 0 NOT NULL
        )`,
    (err :any) => {
        if (err) {
            console.log('Table already created')
        }else{
            console.log('Table Created')
            const insert = db.prepare('INSERT INTO pickup_appointments (title, author, edition_number, pickup_time) VALUES (?,?,?,?)')
            let title = 'Henry Moore'
            let author = 'Henry Moore'
            let edition_number = '68008254'
            let pickup_time = getDateTimeString(new Date('2022-11-30T14:20:00.000+07:00'))
            insert.run([title, author, edition_number, pickup_time])
            insert.finalize()
            console.log('Finish inserting data')
        }
    });
}