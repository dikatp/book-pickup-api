import { Response } from "express"

export const getDateTimeString = (date: Date) => {
    const map: any = {
        dd: ('0' + date.getDate()).slice(-2),
        MM: ('0' + (date.getMonth()+1)).slice(-2),
        yyyy: date.getFullYear(),
        HH: ('0' + date.getHours()).slice(-2),
        mm: ('0' + date.getMinutes()).slice(-2),
        ss: ('0' + date.getSeconds()).slice(-2),
    }

    return 'yyyy-MM-dd HH:mm:ss'.replace(/MM|dd|yyyy|HH|mm|ss/gi, matched => map[matched])
}

export const dataNotFound = (response: Response, err: string) => {[
    response.status(404).json({message: err})
]}

export const badRequest = (response: Response, err: string) => {
    response.status(400).json({message: err})
}

export const internalError = (response: Response, err: Error) => {
    response.status(500).json({message: err.message})
}