import { query, selectFirst } from "../services/database"
import { getDateTimeString } from "../services/helper"

export type PickupAppointment = {
    id: number,
    title: string,
    author: string,
    edition_number: string,
    pickup_time: string,
    is_deleted: string
}

const getPickupAppointments = async (offset :any, limit :any) => {
    let pickupAppointment = await query(`
        SELECT *
        FROM pickup_appointments
        WHERE (is_deleted IS NULL or is_deleted != 'true')
        ORDER BY pickup_time ASC, created_at ASC LIMIT ?, ?
    `, [offset, limit])
    return pickupAppointment as PickupAppointment[]
}

const getTotal = async () => {
    let total = await selectFirst(`
        SELECT count(*) as total
        FROM pickup_appointments
        WHERE (is_deleted IS NULL or is_deleted != 'true')
    `)
    return total['total']
}

const getPickupAppointment = async (id: number) => {
    let pickupAppointment = await selectFirst(`
        SELECT *
        FROM pickup_appointments
        WHERE (is_deleted IS NULL or is_deleted != 'true')
        AND id = ?
    `, [id])
    return pickupAppointment as unknown as PickupAppointment
}

const getExistingPickupAppointment = async (pickupAppointment :PickupAppointment) => {
    let existingPickupAppointment = await selectFirst(`
        SELECT *
        FROM pickup_appointments
        WHERE (is_deleted IS NULL or is_deleted != 'true')
        and title = ?
        and author = ?
        and edition_number = ?
        and pickup_time = ?
    `, [
        pickupAppointment.title,
        pickupAppointment.author,
        pickupAppointment.edition_number,
        getDateTimeString(new Date(pickupAppointment.pickup_time+'+07:00'))
    ])
    return existingPickupAppointment as PickupAppointment
}

const insertPickupAppointment = async (pickupAppointment :PickupAppointment) => {
    await query(`
        INSERT INTO pickup_appointments (title, author, edition_number, pickup_time)
        VALUES (?,?,?,?)
    `, [
        pickupAppointment.title,
        pickupAppointment.author,
        pickupAppointment.edition_number,
        getDateTimeString(new Date(pickupAppointment.pickup_time+'+07:00'))
    ])

    let insertedPickupAppointment = await query(`
        SELECT seq AS id
        FROM sqlite_sequence
        WHERE name='pickup_appointments'
    `)
    return getPickupAppointment(insertedPickupAppointment[0].id)
}

const updatePickupAppointment =async (pickupAppointment: PickupAppointment) => {
    await query(`
        UPDATE pickup_appointments
        set title = ?,
        author = ?,
        edition_number = ?,
        pickup_time = ?
        WHERE (is_deleted IS NULL or is_deleted != 'true')
        AND id = ?
    `, [
        pickupAppointment.title,
        pickupAppointment.author,
        pickupAppointment.edition_number,
        getDateTimeString(new Date(pickupAppointment.pickup_time+'+07:00')),
        pickupAppointment.id
    ])

    return getPickupAppointment(pickupAppointment.id)
}

const deletePickupAppointment = async (id: number) => {
    await query(`
        UPDATE pickup_appointments
        set is_deleted = 'true'
        WHERE (is_deleted IS NULL or is_deleted != 'true')
        AND id = ?
    `, [id])
}

export const pickupAppointmentModel = {
    getPickupAppointments,
    getTotal,
    getPickupAppointment,
    getExistingPickupAppointment,
    insertPickupAppointment,
    updatePickupAppointment,
    deletePickupAppointment
}