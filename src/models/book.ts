import axios from 'axios'

type Author = {
    key: string,
    name: string,
    birth_date: string,
}

type Book = {
    key: string,
    title: string,
    cover_key: string,
}

type GetBooksResponse = {
    key: string,
    name: string,
    subject_type: string,
    work_count: number,
    works: any[]
}

type GetBookDetailResponse = {
    key: string,
    title: string,
    publishers: string[],
    authors: Author[],
    number_of_pages: number,
    publish_places: string[],
    latest_revision: number,
    lccn: string[],
    isbn_10: string[],
    isbn_13: string[],
}

type Books = {
    books: Book[],
    page: number,
    limit: number,
    total: number,
    total_page: number,
    total_display: number,
}

type BookDetail = {
    key: string,
    title: string,
    publisher: string,
    author: Author,
    number_of_pages: number,
    publish_places: string[],
    latest_revision: number,
    lccn: string,
    isbn_10: string,
    isbn_13: string,
}

async function getBooks(subject :string, limit :number, page :number) {
    try {
        page = page
        limit = limit
        let offset = (page - 1) * limit
        const { data, status } = await axios.get<GetBooksResponse>(
            'https://openlibrary.org/subjects/'+ subject +'.json?limit=' + limit + '&offset=' + offset,
            {
                headers: {
                    Accept: 'application/json',
                },
            },
        )
        console.log('response status is: ', status)

        let books = new Array<Book>()
        data.works.forEach((work) => {
            if(work.cover_edition_key !== null) {
                books.push({
                    key: work.key.replace('/works/', ''),
                    title: work.title,
                    cover_key: work.cover_edition_key.replace('/works/', ''),
                })
            }
        })

        return {
            books: books,
            page: page,
            limit: limit,
            total: data.work_count,
            total_page: Math.ceil(data.work_count / limit),
            total_display: books.length
        } as Books
    } catch (error) {
        if (axios.isAxiosError(error)) {
            console.log('error message: ', error.message)
            return error.message
        } else {
            console.log('unexpected error: ', error)
            return 'An unexpected error occurred'
        }
    }
}

async function getBook(cover_edition_key: string) {
    try {
        let getBookDetail = await axios.get<GetBookDetailResponse>(
            'https://openlibrary.org/books/'+ cover_edition_key +'.json',
            {
                headers: {
                    Accept: 'application/json',
                },
            },
        )
        console.log('response status is: ', getBookDetail.status)

        let tmpAuthor = getBookDetail.data.authors ? getBookDetail.data.authors[0] : {key: '', name: ''}
        const getAuthor = await axios.get<Author>(
            'https://openlibrary.org/authors/'+ tmpAuthor.key.replace('/authors/', '') +'.json',
            {
                headers: {
                    Accept: 'application/json',
                },
            },
        )
        console.log('response status is: ', getAuthor.status)

        let author :Author = {
            key: getAuthor.data.key,
            name: getAuthor.data.name,
            birth_date: getAuthor.data.birth_date,
        }
        let bookDetail = {
            key: getBookDetail.data.key,
            title: getBookDetail.data.title,
            publisher: getBookDetail.data.publishers ? getBookDetail.data.publishers[0] : '',
            author: author,
            number_of_pages: getBookDetail.data.number_of_pages,
            publish_places: getBookDetail.data.publish_places,
            latest_revision: getBookDetail.data.latest_revision,
            lccn: getBookDetail.data.lccn ? getBookDetail.data.lccn[0] : '',
            isbn_10: getBookDetail.data.isbn_10 ? getBookDetail.data.isbn_10[0] : '',
            isbn_13: getBookDetail.data.isbn_13 ? getBookDetail.data.isbn_13[0] : '',
        }
        return bookDetail as BookDetail
    } catch (error) {
        if (axios.isAxiosError(error)) {
            console.log('error message: ', error.message)
            return error.message
        } else {
            console.log('unexpected error: ', error)
            return 'An unexpected error occurred'
        }
    }
}

export const bookModel = {
    getBooks,
    getBook,
}