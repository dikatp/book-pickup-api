import { Application, Router } from "express";
import { bookRouter } from "./books";
import { pickupAppointmentRouter } from "./pickup_appointments"

export const useRoutes = (app: Application) => {
    const apiRouter = Router()
    apiRouter.use('/books', bookRouter)
    apiRouter.use('/pickup_appointments', pickupAppointmentRouter)

    app.use('/api/v1', apiRouter)
}