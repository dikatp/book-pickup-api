import { Router } from "express"
import { bookController } from "../controllers/book"

const bookRouter = Router()
bookRouter.get('/', bookController.getBooks)
bookRouter.get('/:id', bookController.getBook)

export {
    bookRouter
}