import { Router } from "express"
import { pickupAppointmentController } from "../controllers/pickup_appointment"

const pickupAppointmentRouter = Router()
pickupAppointmentRouter.get('/', pickupAppointmentController.getPickupAppointments)
pickupAppointmentRouter.get('/:id', pickupAppointmentController.getPickupAppointment)
pickupAppointmentRouter.post('/', pickupAppointmentController.insertPickupAppointment)
pickupAppointmentRouter.put('/:id', pickupAppointmentController.updatePickupAppointment)
pickupAppointmentRouter.delete('/:id', pickupAppointmentController.deletePickupAppointment)

export {
    pickupAppointmentRouter
}