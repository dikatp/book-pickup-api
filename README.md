## Online Book Pickup API
This app is a part of project assignment for recruitment

# Common setup
Clone the repo and install the dependencies.

```bash
git clone https://github.com/DikaTP/book-pickup-api.git
cd book-pickup-api
```

```bash
npm install
```

To start the server, run the following

```bash
npm start
```

# Endpoints (If run in local)
- GET http://localhost:8080/v1/api/books?subject={subject_keyword}
- GET http://localhost:8080/v1/api/books/{cover_key}

- POST http://localhost:8080/v1/api/pickup_appointments
- GET http://localhost:8080/v1/api/pickup_appointments
- GET http://localhost:8080/v1/api/pickup_appointments/{id}
- POST http://localhost:8080/v1/api/pickup_appointments
- PUT http://localhost:8080/v1/api/pickup_appointments/{id}
- DELETE http://localhost:8080/v1/api/pickup_appointments/{id}